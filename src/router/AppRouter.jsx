import React from "react";
import { Route, Routes } from "react-router-dom";

import Login from "../components/login/Login";

import { PublicRoute } from "./PublicRoute";
import { PrivateRoute } from "./PrivateRoute";
import Dashboard from "../components/Dashboard/Dashboard";

export const AppRouter = () => {
  return (
    <div>
      <>
        <Routes>
          <Route
            path="/login"
            element={
              <PublicRoute>
                <Routes>
                  <Route path="/*" element={<Login />} />
                </Routes>
              </PublicRoute>
            }
          />
          <Route
            path="/*"
            element={
              <PrivateRoute>
                <Dashboard />
              </PrivateRoute>
            }
          />
        </Routes>
      </>
    </div>
  );
};
