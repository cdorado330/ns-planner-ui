import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

export const PublicRoute = ({ children }) => {
  const { isAuth } = useSelector((state) => state.auth);

  return !isAuth ? children : <Navigate to="/dashboard" />;
};
