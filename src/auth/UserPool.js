import { CognitoUserPool } from "amazon-cognito-identity-js";

const poolData = {
  UserPoolId: "us-east-1_iXrroiO1G",
  ClientId: "7h4rma036a35bk9b6j0a1bf3f7",
};

export default new CognitoUserPool(poolData);
