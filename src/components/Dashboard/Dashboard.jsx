import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Typography from "@mui/material/Typography";
import { fetchData } from "../../utils/fetchRequest";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import Box from "@mui/material/Box";
import SpeedDial from "@mui/material/SpeedDial";
import SpeedDialIcon from "@mui/material/SpeedDialIcon";
import SpeedDialAction from "@mui/material/SpeedDialAction";

const Dashboard = () => {
  const [employees, setEmployees] = useState([]);
  const [employeesTemp, setEmployeesTemp] = useState([]);
  const [counter, setCounter] = useState("");
  const { formData } = useSelector((state) => state.auth);

  useEffect(() => {
    const getEmployees = async () => {
      const response = fetchData(
        `${process.env.REACT_APP_NS_PLANNER_EMPLOYEE_ENDPOINT}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const responseData = await response;
      console.log("data::", responseData);
      setEmployees(responseData.data);
    };
    getEmployees();
  }, []);
  useEffect(() => {
    getEmployees();
  }, [counter]);

  const getEmployees = async () => {
    const response = fetchData(
      `${process.env.REACT_APP_NS_PLANNER_EMPLOYEE_ENDPOINT}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const responseData = await response;
    console.log("data::", responseData);
    setEmployees(responseData.data);
  };

  const handleCreate = () => {
    const createEmployee = async () => {
      const data = {
        first_name: "Harry",
        last_name: "Potter",
      };

      const response = fetchData(
        `${process.env.REACT_APP_NS_PLANNER_CREATE_EMPLOYEE_ENDPOINT}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          data,
        }
      );
      getEmployees();
    };
    createEmployee();
  };

  return (
    <>
      <List
        sx={{
          width: "100%",
          height: "100%",
          maxWidth: 520,
          bgcolor: "background.paper",
        }}
      >
        {employees &&
          employees.map((employee) => (
            <ListItem key={employee.id}>
              <ListItemAvatar>
                <Avatar>
                  <ImageIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={employee.first_name}
                secondary={employee.last_name}
              />
            </ListItem>
          ))}
      </List>
      <Box sx={{ height: 320, transform: "translateZ(0px)", flexGrow: 1 }}>
        <SpeedDial
          ariaLabel="SpeedDial basic example"
          sx={{ position: "absolute", bottom: 16, right: 16 }}
          icon={<SpeedDialIcon />}
          onClick={handleCreate}
        ></SpeedDial>
      </Box>
    </>
  );
};

export default Dashboard;
