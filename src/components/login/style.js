import styled from "styled-components";

export const LoginContainer = styled.main`
  display: grid;
  width: 100%;
  height: 100vh;
  justify-items: center;
  align-items: center;
  .nearsure-google-login-container {
    border: 1px solid lightgrey;
    border-radius: 10px;
    width: 400px;
    height: 450px;
    align-items: center;
    justify-items: center;
    display: grid;
  }
  .nearsure-login-img-logo {
    width: 50%;
  }

  .nearsure-login-button {
    display: grid;
    align-self: start;
  }
`;
