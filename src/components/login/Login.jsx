import React, { useState } from "react";

import UserPool from "../../auth/UserPool";
import { AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import { LoginContainer } from "./style";
import logo from "../../static/assets/img/logo.png";
import { MutatingDots } from "react-loader-spinner";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { isAuth, addValue } from "../../store/slices/auth/authSlice";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

const Login = () => {
  const [loader, setLoader] = useState(false);
  const [userEmail, setUserEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useNavigate();
  const { auth } = useSelector((state) => state);

  const dispatch = useDispatch();

  const onSubmit = (e) => {
    e.preventDefault();
    setLoader(true);
    const user = new CognitoUser({
      Username: userEmail,
      Pool: UserPool,
    });
    const authDetails = new AuthenticationDetails({
      Username: userEmail,
      Password: password,
    });
    user.authenticateUser(authDetails, {
      onSuccess: (data) => {
        console.log("logueado", data);
        dispatch(isAuth());
        dispatch(
          addValue({
            role: data.accessToken.payload["cognito:groups"][0],
            token: data.accessToken.jwtToken,
            email: data.idToken.payload.email,
          })
        );
        history("dashboard", {
          state: {
            roles: data.accessToken.payload["cognito:groups"],
          },
        });
        setLoader(false);
      },
      onFailure: (err) => {
        setLoader(false);
        console.log("onfailure", err);
      },
    });
  };
  return (
    <LoginContainer>
      <div className="nearsure-google-login-container">
        <div className="nearsure-login-logo">
          <img src={logo} alt="" className="nearsure-login-img-logo" />
        </div>
        <div className="nearsure-login-button">
          {!loader ? (
            <form onSubmit={onSubmit}>
              <Stack spacing={2}>
                <TextField
                  id="email"
                  label="Email"
                  variant="outlined"
                  value={userEmail}
                  onChange={(e) => setUserEmail(e.target.value)}
                  type="email"
                />
                <TextField
                  id="password"
                  label="Password"
                  type="password"
                  variant="outlined"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                <Button variant="outlined" type="submit">
                  Login
                </Button>
              </Stack>
            </form>
          ) : (
            <div className="loader">
              <MutatingDots
                ariaLabel="loading-indicator"
                width={90}
                height={90}
                color="#00D0F5"
                secondaryColor="#010554"
              />
            </div>
          )}
        </div>
      </div>
    </LoginContainer>
  );
};

export default Login;
