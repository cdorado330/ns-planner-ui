import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  auth: false,
  formData: [],
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    isAuth: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.auth = true;
    },
    addValue: (state, action) => {
      state.formData.push(action.payload);
    },
    
  },
});

// Action creators are generated for each case reducer function
export const { isAuth, addValue } = authSlice.actions;
